# Flow Auth plugin for Craft CMS 3.x

NOTE: This plugin is unsupported and is used for internal projects.

Log in to the CraftCMS control panel with Google Auth

## Requirements

This plugin requires Craft CMS 3.x

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer config repositories.flowauth vcs "https://bitbucket.org/flowsa/flow-auth.git"
        composer require "flowsa/flow-auth @dev"

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Flow Auth.

## Flow Auth Overview

Flow Auth allows users to log into the control panel using their Google account. The google account must match their registered email address. Alternatively, you may set any user from your Google Suite domain to be able to log in with the user account.

## Configuring Flow Auth

Because we rely on only a Google Auth login and won't have a password, set [elevatedSessionDuration](https://docs.craftcms.com/v2/config-settings.html#elevatedsessionduration
) to false in general.php

Once installed, following the instructions on the settings page to configure your Google Sign In client.

## Using Flow Auth

You can create a login page using front-end templates and the variable `craft.flowAuth.loginUrl`

Alternatively, set a default route in the plugin settings and visit it here: `/adminlogin`

## Flow Auth Roadmap

- Allow whitlisting of multiple domains
- Deal with elevated privilege issue more elegantly

Icon made by Freepik from www.flaticon.com 
