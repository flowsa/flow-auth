<?php
/**
 * Flow Auth plugin for Craft CMS 3.x
 *
 * Oauth 2 / Google 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

/**
 * Flow Auth en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('flow-auth', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Richard Frank
 * @package   FlowAuth
 * @since     1.0.0
 */
return [
    'Flow Auth plugin loaded' => 'Flow Auth plugin loaded',
];
