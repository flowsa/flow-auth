<?php
/**
 * Flow Auth plugin for Craft CMS 3.x
 *
 * Oauth 2 / Google 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\flowauth\controllers;

use flowsa\flowauth\FlowAuth;

use Craft;
use craft\web\Controller;
use craft\web\View;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Richard Frank
 * @package   FlowAuth
 * @since     1.0.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'login', 'logout'];

    // Public Methods
    // =========================================================================

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/flow-auth/default
     *
     * @return mixed
     */
    public function actionIndex()
    {


        $oldMode = \Craft::$app->view->getTemplateMode();
        \Craft::$app->view->setTemplateMode(View::TEMPLATE_MODE_CP);
        $html = \Craft::$app->view->renderTemplate('flow-auth/loginpage');


        \Craft::$app->view->setTemplateMode($oldMode);


return $html;

    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/flow-auth/default/do-something
     *
     * @return mixed
     */
    public function actionLogin()
    {

        $result = FlowAuth::$plugin->flowAuthService->loginService();

        if ($result) {

            echo "Success, redirecting";

            // TODO make dashboard url dynamic
            Craft::$app->response->redirect("/admin/dashboard");
            return Craft::$app->end();

        } else {

            echo "Something went wrong";
            exit;

        }

    }

    public function actionLogout()
    {

        $result = FlowAuth::$plugin->flowAuthService->logoutService();

echo "Logged out";exit;

        if($result) {
          echo "Logged out";exit;
        }



        // return $result;
    }





}
