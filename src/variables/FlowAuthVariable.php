<?php
/**
 * Flow Auth plugin for Craft CMS 3.x
 *
 * Oauth 2 / Google 
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\flowauth\variables;

use flowsa\flowauth\FlowAuth;

use Craft;
use craft\elements\User;
use craft\helpers\UrlHelper;

/**
 * Flow Auth Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.flowAuth }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Richard Frank
 * @package   FlowAuth
 * @since     1.0.0
 */
class FlowAuthVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.flowAuth.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.flowAuth.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function allUsers()
    {

        $users = User::find();
        
        $users->can('accessCp')->all();

        $userArray = [];

        foreach($users as $user) {

          $userArray[$user->id] = $user->id . ". " . $user->firstName . $user->lastName;

        }

        return $userArray;
    }

    public function callbackUrl() {
      return UrlHelper::actionUrl('flow-auth/default/login');
    }

    public function loginUrl() {
      return UrlHelper::actionUrl('flow-auth/default/login');
    }

    public function logoutUrl() {
      return UrlHelper::actionUrl('flow-auth/default/logout');
    }

}
