<?php
/**
 * Flow Auth plugin for Craft CMS 3.x
 *
 * Oauth 2 / Google
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2018 Richard Frank
 */

namespace flowsa\flowauth\services;

use Craft;
use craft\base\Component;
use flowsa\flowauth\FlowAuth;
use craft\helpers\UrlHelper;
use craft\elements\User;
use yii\base\ErrorException;
use yii\base\Exception;

/**
 * FlowAuthService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Richard Frank
 * @package   FlowAuth
 * @since     1.0.0
 */
class FlowAuthService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     FlowAuth::$plugin->flowAuthService->exampleService()
     *
     * @return mixed
     */
    public function loginService() {

        Craft::getLogger()->log('Login Service Initiated', \yii\log\Logger::LEVEL_INFO, 'flow-auth');


        $redirectUrl = UrlHelper::actionUrl('flow-auth/default/login');

        $provider = new \League\OAuth2\Client\Provider\Google([
            'clientId' => FlowAuth::getInstance()->settings->googleClientId,
            'clientSecret' => FlowAuth::getInstance()->settings->googleClientSecret,
            'redirectUri' => $redirectUrl,
            'hostedDomain' => FlowAuth::getInstance()->settings->hostedDomain,
        ]);

        // baseCpUrl

        if (!empty($_GET['error'])) {

            $error = 'Got error: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8');

            Craft::getLogger()->log('Google responded with error:', \yii\log\Logger::LEVEL_ERROR, 'flow-auth');

            // Got an error, probably user denied access
            exit($error);

        } elseif (empty($_GET['code'])) {

            Craft::getLogger()->log('Authorisation code not found, getting one', \yii\log\Logger::LEVEL_INFO, 'flow-auth');

            // If we don't have an authorization code then get one
            $authUrl = $provider->getAuthorizationUrl();
            $_SESSION['oauth2state'] = $provider->getState();
            header('Location: ' . $authUrl);
            exit;

        } 

        // REMIND ME TO ADD THIS BACK
        
        // elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

        //     // State is invalid, possible CSRF attack in progress
        //     unset($_SESSION['oauth2state']);
        //     exit('Invalid state');

        // } 
        
        else {

            // Try to get an access token (using the authorization code grant)
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code'],
            ]);

            try {

                // We got an access token, let's now get the owner details
                $googleUser = $provider->getResourceOwner($token);

                Craft::getLogger()->log('Google user authenticated ' . $googleUser->getEmail(), \yii\log\Logger::LEVEL_INFO, 'flow-auth');

                // Allow any account within a particular domain to login as that user
                $matchAnyAccount = FlowAuth::getInstance()->settings->matchAnyAccountInDomain;

                $domainRestrictionIsNotBlank = FlowAuth::getInstance()->settings->hostedDomain != "" ? true : false;

                if ($matchAnyAccount && $domainRestrictionIsNotBlank) {

                  // Login as this user
                  $loginUserId = FlowAuth::getInstance()->settings->userIdToLoginAs;

                  Craft::getLogger()->log('Match any account and login as user id ' . $loginUserId, \yii\log\Logger::LEVEL_INFO, 'flow-auth');

                  return Craft::$app->user->loginByUserId($loginUserId);

                } else {

                   // get email address from Google account
                  $email = $googleUser->getEmail();

                  Craft::getLogger()->log('Match specific account and login as craft user ' . $email, \yii\log\Logger::LEVEL_INFO, 'flow-auth');

                  $user = Craft::$app->users->getUserByUsernameOrEmail($email);

                  if($user !== null ) {
                    
                    return Craft::$app->user->loginByUserId($user->id);

                  } else {

                    $message = "User with this email address could not be found";

                    Craft::getLogger()->log($message, \yii\log\Logger::LEVEL_INFO, 'flow-auth');
                    throw new ErrorException($message);
                    
                  }


                }

            } catch (ErrorException $e) {

              $message = 'Something went wrong: ' . $e->getMessage();

              Craft::getLogger()->log($message, \yii\log\Logger::LEVEL_ERROR, "flow-auth");
              throw new ErrorException($message);

            }

        }

    }



     public function logoutService() {

      // TODO: Trying to revoke the session on Google itself - need to find out how to do that

      // session_destroy();

      Craft::getLogger()->log("Logging out", \yii\log\Logger::LEVEL_INFO, 'flow-auth');

      $result = Craft::$app->user->logout();

      return $result;


     }



}
