/**
 * Flow Auth plugin for Craft CMS
 *
 * Flow Auth JS
 *
 * @author    Richard Frank
 * @copyright Copyright (c) 2018 Richard Frank
 * @link      www.flowsa.com
 * @package   FlowAuth
 * @since     1.0.0
 */
